<?php
	$to = new DateTIme();
	$from = new DateTime('1880-09-09');
	$diff = $to->diff($from);
?>


<div class="row" style="margin-top: 10px;">
    <div class="col-md-12" style="padding: 0;">
        <div id="carousel-top-page" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-top-page" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-top-page" data-slide-to="1"></li>
                <li data-target="#carousel-top-page" data-slide-to="2"></li>
                <li data-target="#carousel-top-page" data-slide-to="3"></li>
                <li data-target="#carousel-top-page" data-slide-to="4"></li>
                <li data-target="#carousel-top-page" data-slide-to="5"></li>
            </ol>

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <?php echo_responsive_image_tag('index-kv-itamae.jpg', '板前が包丁で手切り'); ?>
                    <div class="carousel-caption">
                        <?php echo is_ja() ? '板前が包丁で手切り' : 'Hand-slicing Matsusaka beef by a certified chef'; ?>
                    </div>
                </div>
                <div class="item">
                    <?php echo_responsive_image_tag('index-kv-wall.jpg', '壁画'); ?>
                    <div class="carousel-caption" class="img-responsive">
                        <?php echo is_ja() ? '壁画' : 'Wall painting "Wagyu & Grape"'; ?>
                    </div>
                </div>
                <div class="item">
                    <?php echo_responsive_image_tag('index-kv-table.jpg', '卓子席'); ?>
                    <div class="carousel-caption" class="img-responsive">
                        <?php echo is_ja() ? '卓子席' : 'Table seat'; ?>
                    </div>
                </div>
                <div class="item">
                    <?php echo_responsive_image_tag('index-kv-zashiki.jpg', '掘り炬燵式のお座敷'); ?>
                    <div class="carousel-caption">
                        <?php echo is_ja() ? '掘り炬燵式のお座敷' : 'Zashiki(tatami) room with sunken kotatsu'; ?>
                    </div>
                </div>
                <div class="item">
                    <?php echo_responsive_image_tag('index-kv-ume.jpg', 'すき焼 梅（もも肉）'); ?>
                    <div class="carousel-caption">
                        <?php echo is_ja() ? 'すき焼 梅（もも肉）' : 'UME TEISHOKU (plum course)'; ?>
                    </div>
                </div>
                <div class="item">
                    <?php echo_responsive_image_tag('index-kv-matsu.jpg', 'すき焼 松（ロース肉）'); ?>
                    <div class="carousel-caption">
                        <?php echo is_ja() ? 'すき焼 松（ロース肉）' : 'MATSU TEISHOKU (pine course)'; ?>
                    </div>
                </div>
            </div>

            <a class="left carousel-control" href="#carousel-top-page" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-top-page" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<div class="row" style="background:#ebe1da;color: #462300;
    background-image: url(<?php echo get_image_path("logo-chipped.png"); ?>);
    background-position: right 0 bottom 0;
    background-repeat: no-repeat;padding: 30px;margin-bottom: 10px;">
    <div class="col-md-10 col-md-offset-1">
        <strong class="text-center" style="display: block;font-size: 2.5em;padding: 30px 0;">
            <?php if (is_ja()) : ?>
                今も変わらぬ味。<br>松阪牛の旨味と今朝秘伝の割下
            <?php else : ?>
                The Century's Tradition<br>
                True Taste of Matsusaka Beef<br>
                with Special Warishita Sauce
            <?php endif; ?>
        </strong>
        <div style="font-size: 1.2em; line-height: 2em;padding: 30px 0;">
            <?php if (is_ja()) : ?>
			<p>〈今朝〉と書いて「イマアサ」と読みます。すき焼一筋<?php echo $diff->format('%y'); ?>年。</p>
                <p>
                    最高級ランクA5の松阪牛を中心とした黒毛和牛を板前が一枚一枚包丁で切り分け、
                    今朝秘伝の割下（タレ）に根深の白い千寿葱、椎茸、焼豆腐など吟味した
                    ザク（野菜）を使用しております。</p>
            <?php else : ?>
                <p>We have <?php echo $diff->format('%y'); ?> years' history for serving Sukiyaki.</p>
                <p>Modern taste is still important for you as well as our proud tradition and history.</p>
                <p>We serve specially selected Matsusaka-beef, Wari-shita (sauces), leek, shiitake mushrooms, Shirataki
                    noodle and sophisticated vegetables.</p>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="row" style="margin-bottom: 10px;">
	<div class="col-xs-12 text-center">
		<a href="https://chienotomoshibi.jp/intro/imaasa/" class='hidden-md hidden-lg'
		   style="display: inline-block;margin-top: 10px;" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/img/chienotomoshibi_xlarge.jpg"
				 alt="智慧の燈火 130年余りの歴史を刻んできた明治から伝わる今朝の味わい" class='img-responsive'>
		</a>
		<a href="https://chienotomoshibi.jp/intro/imaasa/" class='hidden-xs hidden-sm'
		   style="display: inline-block;margin-top: 10px;" target="_blank">
			<img src="<?php echo get_template_directory_uri(); ?>/img/chienotomoshibi_large.jpg"
				 alt="智慧の燈火 130年余りの歴史を刻んできた明治から伝わる今朝の味わい" class='img-responsive'>
		</a>
    </div>
</div>
<div class="row" style="background: #fff;border: solid 15px #5b4635;margin-top: 10px; margin-bottom: 10px;">
    <div class="col-md-12" style="padding: 10px 30px;">
        <h2 style="font-size: 2em;line-height: 2em;text-indent: 1em;"><?php echo is_ja() ? 'お知らせ' : 'News' ?></h2>
        <hr style="margin: 0;border-top: 1px solid #999;">
        <?php $args = array(
            'numberposts' => 3,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $posts = get_posts($args); ?>
        <ul class="list-unstyled" style="line-height: 3em;">
            <?php foreach ($posts as $post): ?>
                <li style="border-bottom: 1px dashed;">
                    <time style="display: inline-block; margin: 0 20px;">
                        <?php
                        if (is_ja()) :
                            echo get_the_date("Y年n月j日");
                        else :
                            echo get_the_date("F jS, Y");
                        endif;
                        ?>
                    </time>
                    <a style="display: inline-block; margin: 0 20px;" href="<?php the_permalink(); ?>">
                        <?php if (day_diff(date("Y-m-d"), get_the_date('Y-m-d')) <= LIMIT_POST_FRESH) : ?>
                            <span class="new">[NEW] </span>
                        <?php endif; ?>
                        [<?php echo get_the_category()[0]->name; ?>]
                        <?php the_title(); ?>
                    </a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="row text-center"
     style="background: #fff;border: solid 3px #5b4635;margin-top: 10px; margin-bottom: 10px;padding: 30px;">
    <div class="col-md-4">
        <a href="<?php echo home_url("about"); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("index-sub-about.jpg", "今朝について"); ?>
        </a><br>
        <a href="<?php echo home_url("about"); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("subtitle_index_about.jpg", "今朝について", true); ?>
        </a>
        <p class="text-left" style="line-height: 2em;">
            <?php if (is_ja()) : ?>
                明治13（1880）年に創業。<br>
                初代店主、藤森今朝次郎が修業したのは、かつて銀座煉瓦街に在った「今廣」でした。<br>
                自身の名前と暖簾分けにより今の字を貰い〈今朝〉と名付けられました。
            <?php else : ?>
                We established in 1880(Meiji era).<br>
                Kesajiro Fujimori who first opened his restaurant here has trained on sukiyaki restaurant "Ima-Hiro" in Ginza Brick Street.
                <br>
                After that he named his own restaurant "Ima-Asa".<br>
                The name means franchise of his former restaunrat "Ima"and his name"Asa" (as same meaning "Kesa").<br>
                Now let's look inside the honorable history.
            <?php endif; ?>
        </p>
    </div>
    <div class="col-md-4">
        <a href="<?php echo home_url('menu/dinner'); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("index-sub-menu.jpg", "メニュー"); ?>
        </a><br>
        <a href="<?php echo home_url("menu/dinner"); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("subtitle_index_menu.jpg", "メニュー", true); ?>
        </a>
        <p class="text-left" style="line-height: 2em;">
            <?php if (is_ja()) : ?>
                ご夕食はすき焼やしゃぶしゃぶ、オイル焼などメニューを揃えております。<br>
                ご昼食もリーズナブルなランチから、ご会食向けの定食までございます。
            <?php else : ?>
                We have various menus centering on sukiyaki and shabu-shabu.<br>
                You can enjoy lunch as well as dinner.<br>
                If you take a glance in advance.<br>
                You will enjoy with feeling our <?php echo $diff->format('%y'); ?> years' history and tradition.
            <?php endif; ?>
        </p>
    </div>
    <div class="col-md-4">
        <a href="<?php echo home_url('reservation'); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("index-sub-reserve.jpg", "予約"); ?>
        </a><br>
        <a href="<?php echo home_url("reservation"); ?>"
           style="display: inline-block;margin: 20px 0;">
            <?php echo_responsive_image_tag("subtitle_index_reserve.jpg", "予約", true); ?>
        </a>
        <p class="text-left" style="line-height: 2em;">
            <?php if (is_ja()) : ?>
                すき焼〈今朝〉ではWebまたはお電話でのご予約を承っております。<br>
                掘り炬燵式のお座敷、大広間、テーブル席まで、ご用途に応じてご指定ください。
            <?php else : ?>
                We welcome your booking by web or phone.<br>
                It's preferable to reserve in advance because reserved guest is prior to ones not reserved.
            <?php endif; ?>
        </p>
    </div>
</div>
<div class="row" style="background: #8c873e;color: #fff;margin: 10px -15px;padding: 10px;">
    <div class="col-md-5 text-center" style="margin: 10px auto;">
        <?php echo_responsive_image_tag('index_shop_name.png', 'すき焼今朝', false, "margin: 10px auto;"); ?>
        <p style="display: inline-block;line-height: 2em;">
            <?php if (is_ja()) : ?>
                東京都港区東新橋1-1-21 今朝ビル2階
            <?php else : ?>
                Imaasa Building 2F, Higashi-Shimbashi 1-1-21, Minato District, Tokyo, Japan
            <?php endif; ?>
        </p>
        <a href="tel:0335725286" style="color: #000; display: inline-block; font-family: serif; font-size: 2em; font-weight: bold;">03-3572-5286</a>
        <?php if (is_ja()) : ?>
            <p style="display: inline-block;line-height: 2em;">ランチタイム: 11:30～14:00</p>
            <p style="display: inline-block;line-height: 2em;">ディナータイム: 17:30～21:30 (L.O 21:00)</p>
            <p style="display: inline-block;line-height: 2em;">月曜日から金曜日営業、土日祝日休業<br>（但し、12月の土曜日のみ営業）</p>
        <?php else : ?>
            <p style="display: inline-block;line-height: 2em;">Lunch Time: 11:30～14:00</p>
            <p style="display: inline-block;line-height: 2em;">Dinner Time: 17:30～21:30 (L.O 21:00)</p>
            <p style="display: inline-block;line-height: 2em;">
                Opened from Mon to Fri (Opened Sat ONLY in December)<br>
                Closed on Saturday, Sunday and public holidays
            </p>
        <?php endif; ?>
        <br>
        <a href="<?php echo home_url("reservation"); ?>" style="display: inline-block;">
            <?php echo_responsive_image_tag('btn-reserve.jpg', 'ご予約はこちらから', true); ?>
        </a>
    </div>
    <div class="col-md-7 text-center" style="margin: 10px auto;">
        <div class="hidden-xs" style="height: 30px;"></div>
        <a href="<?php echo home_url("access"); ?>" style="display: inline-block;">
            <?php echo_responsive_image_tag('map.jpg', '地図', false, "margin: 0 auto;"); ?>
        </a>
        <div class="text-right" style="line-height: 3em;">
            <a href="https://www.google.co.jp/maps/place/%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%AF%E5%8C%BA%E6%9D%B1%E6%96%B0%E6%A9%8B%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%E2%88%92%EF%BC%92%EF%BC%91/@35.6663201,139.7605594,17z/data=!3m1!4b1!4m2!3m1!1s0x60188be836d763d5:0x46ce478097bd1a5f"
               target="_blank" style="display: inline-block;">
                <?php echo_responsive_image_tag('arrow.png', '矢印', false, "display:inline-block;vertical-align: middle;"); ?>
            </a>
            <a href="https://www.google.co.jp/maps/place/%E6%9D%B1%E4%BA%AC%E9%83%BD%E6%B8%AF%E5%8C%BA%E6%9D%B1%E6%96%B0%E6%A9%8B%EF%BC%91%E4%B8%81%E7%9B%AE%EF%BC%91%E2%88%92%EF%BC%92%EF%BC%91/@35.6663201,139.7605594,17z/data=!3m1!4b1!4m2!3m1!1s0x60188be836d763d5:0x46ce478097bd1a5f"
               target="_blank" style="display: inline-block;vertical-align: middle;margin: 0 10px;">
                <?php echo is_ja() ? 'Google Mapsで表示' : 'Show in Goolge Map' ; ?>
            </a>
        </div>
    </div>
</div>
