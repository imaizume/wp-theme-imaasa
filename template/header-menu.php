<nav id="toggle" class="visible-xs navbar-toggle noprint">
    <span class="icon-bar" style="background: #01385b;"></span>
    <span class="icon-bar" style="background: #01385b;"></span>
    <span class="icon-bar" style="background: #01385b;"></span>

</nav>
<div id="menu-collapse" class="visible-xs text-center noprint">
    <ul class="table" style="margin: 0; padding: 0;">
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('/'); ?>"><?php echo is_ja() ? 'トップページ' : 'home'; ?></a></li>
        <li role="separator" class="divider"></li>
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('about'); ?>"><?php echo is_ja() ? '今朝について' : 'about'; ?></a></li>
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('menu/dinner'); ?>"><?php echo is_ja() ? 'お品書き' : 'menu'; ?></a></li>
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('reservation'); ?>"><?php echo is_ja() ? 'ご予約' : 'reservation'; ?></a></li>
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('news'); ?>"><?php echo is_ja() ? 'お知らせ' : 'news'; ?></a></li>
        <li class="tr text-uppercase"><a
                href="<?php echo home_url('access'); ?>"><?php echo is_ja() ? '店舗案内' : 'access'; ?></a></li>
    </ul>
</div>


<nav class="hidden-xs noprint">
    <div
        style="background-image: url(<?php echo get_image_path('menu-bg.png'); ?>);background-repeat: repeat-x;background-size:contain;">
        <div class="container">
            <div class="row">
                <a href="<?php echo home_url('about'); ?>"><img
                        src="<?php echo get_image_path('menu-1.png'); ?>" alt=""
                        class="col-xs-2 col-xs-offset-1" style='display: inline-block;padding: 0;'></a>
                <a href="<?php echo home_url('menu/dinner'); ?>"><img
                        src="<?php echo get_image_path('menu-2.png'); ?>" alt="" class="col-xs-2"
                        style='display: inline-block;padding: 0;'></a>
                <a href="<?php echo home_url('reservation'); ?>"><img
                        src="<?php echo get_image_path('menu-3.png'); ?>" alt="" class="col-xs-2"
                        style='display: inline-block;padding: 0;'></a>
                <a href="<?php echo home_url('news'); ?>"><img
                        src="<?php echo get_image_path('menu-4.png'); ?>" alt="" class="col-xs-2"
                        style='display: inline-block;padding: 0;'></a>
                <a href="<?php echo home_url('access'); ?>"><img
                        src="<?php echo get_image_path('menu-5.png'); ?>" alt="" class="col-xs-2"
                        style='display: inline-block;padding: 0;'></a>
            </div>
        </div>
    </div>
</nav>
