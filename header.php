<?php include (TEMPLATEPATH . '/common.php'); ?>

<!doctype html>
<html lang="<?php echo get_lang(); ?>">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo get_image_path("favicon.ico"); ?>" type="image/vnd.microsoft.icon">
    <link rel="apple-touch-icon" href="<?php echo get_image_path("logo.png"); ?>" />
    <title>
        <?php
        $title = "";
        $title = is_ja() ? 'すき焼今朝' : 'Sukiyaki Restaurant Ima-Asa' ;
        if (!is_front_page()) :
            $title = wp_title('') . ' | ' . $title;
        endif;
        echo $title;
        ?>
    </title>
    <link rel="alternate" type="application/rss+xml" title="<?php echo $title; ?> フィード"
          href="<? home_url('/') ?>/feed"/>
    <?php if(is_page(array('booking', 'booking-error'))) : ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');
    echo '?' . filemtime(get_stylesheet_directory() . '/style.css'); ?>" type="text/css"/>

    <?php wp_head(); ?>
</head>
<body>

<header class="noprint">
    <div style="background-image: url(<?php echo get_image_path('header.jpg'); ?>);background-size: cover;background-position-x: 15%;">
        <div class="container">
            <div class="row">
                <div class="text-center"
                     style="padding: 10px;">
                    <span class="visible-xs-inline-block" style="background-color: white;padding: 3px;margin-top: 50px;">Sukiyaki Restaurant IMA-ASA since 1880</span>
                    <span class="hidden-xs"  style="background-color: white;display: inline-block;padding: 3px;">Sukiyaki Restaurant IMA-ASA since 1880</span>
                    <br>
                    <a href="<?php echo home_url('/'); ?>" style="display: inline-block;border-radius: 50%;">
                        <?php echo_responsive_image_tag('logo.png', 'すき焼今朝ロゴ') ?>
                    </a>
                    <br>
                    <a href="<?php echo home_url('/'); ?>" style="display: inline-block;">
                        <?php echo_responsive_image_tag('logo-type.png', 'すき焼今朝') ?>
                    </a>
                    <?php echo do_shortcode('[bogo]'); ?>
                </div>
            </div>
        </div>
    </div>
</header>

<?php if (is_page('confirm')): ?>
    <div style="height: 1em;"></div>
<?php else: ?>
    <?php get_template_part('template/header', 'menu'); ?>
<?php endif; ?>
