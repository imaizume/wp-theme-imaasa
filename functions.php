<?php
if (function_exists('register_sidebar'))
    register_sidebar(array(
            'id' => 'sidebar-1',
            'name' => 'サイドバー1'
        )

    );

add_action('init', function () {
    remove_filter('the_excerpt', 'wpautop');
    remove_filter('the_content', 'wpautop');
});

function override_mce_options($init_array)
{
    global $allowedposttags;

    $init_array['valid_elements'] = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    $init_array['valid_children'] = '+a[' . implode('|', array_keys($allowedposttags)) . ']';
    $init_array['indent'] = true;
    $init_array['wpautop'] = false;
    $init_array['element_format'] = 'HTML';
    $init_array['object_resizing'] = false;
    $style_formats = array(
        array(
            'title' => 'テーブルの見出し',
            'selector' => 'td',
            'classes' => 'th-like'
        ),
        array(
            'title' => '余白無し',
            'selector' => '*',
            'classes' => 'none-margin'
        ),
        array(
            'title' => 'インデント',
            'selector' => 'textarea',
            'indentation' => '20px'
        )
    );
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;
}

add_filter('tiny_mce_before_init', 'override_mce_options');

function get_lang()
{
    return explode("_", get_locale())[0];
}

function is_ja()
{
    return get_lang() == "ja";
}

function echo_responsive_image_tag($file_name, $alt, $lang = false, $style = '')
{
    echo "<img src='" . get_image_path($file_name, $lang) . "' alt='" . $alt . "' class='img-responsive' style='" . $style . "'>";
}

function get_image_path($file_name, $lang = false)
{
    $dir_lang = $lang ? get_lang() . '/' : '';
    return get_resource_url() . '/img/' . $dir_lang . $file_name;
}

function get_script_path($file_name, $vendor = null)
{
    if (!is_null($vendor)) {
        return get_resource_url() . '/' . $vendor . '/' . $file_name;
    } else {
        return get_resource_url() . '/script/' . $file_name;
    }
}

add_theme_support('post-thumbnails');

function new_excerpt_more($more)
{
    global $post;
    return '……(<a href="' . get_permalink() . '">' . __('続きを読む') . '</a>)';
}

add_filter('excerpt_more', 'new_excerpt_more');

function remove_image_attribute($html)
{
    $html = preg_replace('/(width|height)="\d*"\s/', '', $html);
    $html = preg_replace('/class=[\'"]([^\'"]+)[\'"]/i', 'class="img-responsive"', $html);
    return $html;
}

add_filter('image_send_to_editor', 'remove_image_attribute', 10);
add_filter('post_thumbnail_html', 'remove_image_attribute', 10);

function day_diff($date1, $date2)
{

    // 日付をUNIXタイムスタンプに変換
    $timestamp1 = strtotime($date1);
    $timestamp2 = strtotime($date2);

    // 何秒離れているかを計算
    $seconddiff = abs($timestamp2 - $timestamp1);

    // 日数に変換
    $daydiff = $seconddiff / (60 * 60 * 24);

    // 戻り値
    return $daydiff;

}

add_filter('post_thumbnail_html', 'custom_attribute');
function custom_attribute($html)
{
    $myclass = 'img-responsive margin-center';
    return preg_replace('/class=".*\w+"/', 'class="' . $myclass . '"', $html);
}

function shortcode_home_url()
{
    return preg_replace('{/$}', '', home_url('/'));
}

add_shortcode('homeurl', 'shortcode_home_url');

function get_resource_url($lang = false)
{
    return "https://resources.imaasa.com";
}

function shortcode_image_url()
{
    return get_resource_url() . '/img';
}

function shortcode_image_url_lang()
{
    return get_resource_url() . '/img/' . get_lang();
}

//TODO: Inappropriate name of shortcode; these shortcodes will only return image path
add_shortcode('resurl', 'shortcode_image_url');
add_shortcode('resurlloc', 'shortcode_image_url_lang');

//Pagenation
function pagination($pages = '', $range = 2)
{
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) $paged = 1;

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) $pages = 1;
    }

    if ($pages != 1) {
        echo "<div class='table margin-center pagenation' style='width: auto;'>\n";
        echo "<ul class='tr list-inline'>\n";
        if ($paged > 1) {
            echo "<li class='td item prev'><a href='" . get_pagenum_link($paged - 1) . "'>";
            echo "<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>";
            echo "</a></li>\n";
        }

        for ($i = 1; $i <= $pages; $i++) {
            if ($pages != 1 && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<li class='td item active'>" . $i . "</li>\n" : "<li class='td item'><a href='" . get_pagenum_link($i) . "'>" . $i . "</a></li>\n";
            }
        }
        if ($paged < $pages) {
            echo "<li class='td item next'><a href='" . get_pagenum_link($paged + 1) . "'>";
            echo "<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>";
            echo "</a></li>\n";
        }
        echo "</ul>\n";
        echo "</div>\n";
    }
}

/**
 * Append input box to extend column metadatas
 */
add_action( 'cmb2_admin_init', 'cmb2_column_metaboxes' );

function cmb2_column_metaboxes() {

    $prefix = '_column_';

    // メタ情報のボックスを生成
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'コラムのメタ情報', 'cmb2' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true,
    ) );

    // バックナンバー
    $cmb->add_field( array(
        'name'       => __( '号', 'cmb2' ),
        'desc'       => __( '(例: 第64号)', 'cmb2' ),
        'id'         => $prefix . 'backnumber',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
        'default'    => '第XX号',
    ) );

    // 日本語バックナンバー
    $cmb->add_field( array(
        'name'       => __( '発行年', 'cmb2' ),
        'desc'       => __( '(例: 二〇一四年秋号)', 'cmb2' ),
        'id'         => $prefix . 'year-backnumber',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
        'default'    => '二〇XX年Y号',
    ) );

    // コラムのメインタイトル
    $cmb->add_field( array(
        'name' => __( 'メインタイトル', 'cmb2' ),
        'desc' => __( '(例: 今朝通信)', 'cmb2' ),
        'id'   => $prefix . 'maintitle',
        'type' => 'text',
        'default' => '今朝通信',
    ) );

    // コラムのサブタイトル
    $cmb->add_field( array(
        'name' => __( 'サブタイトル', 'cmb2' ),
        'desc' => __( '(例: 汽笛一聲 上機嫌)', 'cmb2' ),
        'id'   => $prefix . 'subtitle',
        'type' => 'text',
        'default' => '汽笛一聲…上機嫌',
    ) );

    // コラムの著者
    $cmb->add_field( array(
        'name' => __( '著者', 'cmb2' ),
        'desc' => __( '(例: すき焼今朝 五代目当主)', 'cmb2' ),
        'id'   => $prefix . 'author',
        'type' => 'text',
        'default' => 'すき焼今朝 五代目当主',
    ) );
}
