<?php get_header(); ?>
<?php $category = get_the_category()[0]; ?>
<?php $slug = $category->slug; ?>
<?php $name = $category->cat_name; ?>

<div class="container" style="margin-top: 10px;">
    <div class="row <?php echo $slug; ?>">
        <div style="margin: 0;overflow-x: hidden">
            <img src="<?php echo get_image_path('title_' . $slug . '.jpg', true); ?>"
                 alt='<?php echo $name; ?>'></div>
        <div class="col-sm-12">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="row" style="padding: 30px;">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1" id="post-<?php the_ID(); ?>"
                         style="margin-top: 10px;">
                        <div>
                            <time style="display: inline-block; margin: 0 20px;">
                                <?php
                                if (is_ja()) :
                                    the_date("Y年n月j日");
                                else :
                                    the_date("F jS, Y");
                                endif;
                                ?>
                            </time>
                        </div>
                        <div class="text-center">
                            <h1><span style="display: inline-block; margin: 0 20px;"
                                      href="<?php the_permalink(); ?>"><?php echo the_title(); ?></span></h1>
                        </div>
                        <?php if ($slug == "column"): ?>

                            <?php $backnumber     = get_post_meta( get_the_ID(), '_column_backnumber', true ); ?>
                            <?php $yearbacknumber = get_post_meta( get_the_ID(), '_column_year-backnumber', true ); ?>
                            <?php $maintitle      = get_post_meta( get_the_ID(), '_column_maintitle', true ); ?>
                            <?php $subtitle       = get_post_meta( get_the_ID(), '_column_subtitle', true ); ?>
                            <?php $author         = get_post_meta( get_the_ID(), '_column_author', true ); ?>

                            <div class="text-right">
                                <?php echo $maintitle; ?> <?php echo $backnumber; ?> <br class="visible-xs-block" />
                                『<?php echo $subtitle; ?>』<br/>
                                <?php echo $author; ?>
                            </div>
                        <?php endif;?>
                        <div style="margin: 30px 0;"><?php the_content(); ?></div>
                        <div class="text-right">
                            <?php if ($slug == "column"): ?>
                                <?php echo $yearbacknumber; ?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
            <div class="row" style="padding: 30px;">
                <div class="col-xs-12 text-center">
                    <a href="<?php echo get_permalink(get_page_by_path($slug)->ID); ?>" class="button-default" style="display: inline-block;padding: 5px;">
                        <?php echo is_ja() ? $name . "一覧へ戻る" : "Back to " . $slug . " list"; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
