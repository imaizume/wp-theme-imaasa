$(function () {
    $.datepicker.setDefaults($.datepicker.regional["ja"]);

    // Holidays until October 2018
    var holidays = ['2016-04-29', '2016-05-03', '2016-05-04', '2016-05-05', '2016-07-18', '2016-08-11', '2016-09-19', '2016-09-22', '2016-10-10', '2016-11-03', '2016-11-23', '2016-12-23', '2017-01-01', '2017-01-02', '2017-01-09', '2017-02-11', '2017-03-20', '2017-04-29', '2017-05-03', '2017-05-04', '2017-05-05', '2017-07-17', '2017-08-11', '2017-09-18', '2017-09-23', '2017-10-09', '2017-11-03', '2017-11-23', '2017-12-23', '2018-01-01', '2018-01-08', '2018-02-11', '2018-02-12', '2018-03-21', '2018-04-29', '2018-04-30', '2018-05-03', '2018-05-04', '2018-05-05', '2018-07-16', '2018-08-11', '2018-09-17', '2018-09-23', '2018-09-24'];

    $('#date').datepicker({
        beforeShowDay: function (date) {
            for (var i = 0; i < holidays.length; i++) {
                var holiday = new Date();
                holiday.setTime(Date.parse(holidays[i]));

                if (holiday.getYear() == date.getYear() &&
                    holiday.getMonth() == date.getMonth() &&
                    holiday.getDate() == date.getDate()) {
                    return [false, 'class-holiday', '祝日'];
                }
            }

            if (date.getDay() == 0) {
                return [false, 'class-sunday', '日曜日'];
            } else if (date.getDay() == 6) {
                if (date.getMonth == 11) {
                    return [true, 'class-saturday', '土曜日'];
                } else {
                    return [false, 'class-saturday', '土曜日'];
                }
            } else {
                return [true, 'class-weekday', '平日'];
            }
        },
        minDate: "+5D",
        maxDate: "+1Y",
        dateFormat: "yy/mm/dd"

    });
});
