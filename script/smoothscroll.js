$(function () {
    $('a[href^="#"]').not(".carousel-control").click(function () {
        var speed = 800;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop: position}, speed, "swing");
        return false;
    });

    var $sb = $("#socialbuttons");
    var textShare = window.document.title;
    $sb.children('.tweet').socialbutton('twitter', {
        button: "horizontal",
        lang: "ja_JP",
        text: textShare,
        url: window.location.href,
    }).width(95).end()
        .children('.like').socialbutton('facebook_like', {
        button: "button_count",
        show_faces: true,
        text: textShare,
        url: window.location.href,
    }).width(110).end()
        .children('.hatena').socialbutton('hatena', {
        button: "standard",
        url: window.location.href,
        title: textShare,
    }).width(70).end()
        .children(".tr").css("height", "30px")
    ;

    $("#toggle").click(function () {
        console.info("on");
        $(this).toggleClass('on');
        return false;
    });
});
