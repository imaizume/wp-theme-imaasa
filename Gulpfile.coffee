bower = require 'gulp-bower'
compass = require 'gulp-compass'
cssnext = require 'gulp-cssnext'
gulp = require 'gulp'
livereload = require 'gulp-livereload'
notify = require 'gulp-notify'
plumber = require 'gulp-plumber'
sass = require 'gulp-sass'

config =
  sassPath: './sass/'
  cssPath: './'
  fontPath: './fonts/'
  bowerDir: './bower_components'

gulp.task 'scss', ()->
  return gulp.src config.sassPath + 'style.scss'
    .pipe plumber()
    .pipe sass()
    .on 'error', (err)->
      console.log(err.message)
    .pipe cssnext()
    .pipe gulp.dest(paths.css)

gulp.task 'compass', ()->
  gulp.src 'sass/style.scss'
    .pipe plumber()
    .pipe compass
      config_file: 'config.rb'
      comments: false
      css: config.cssPath
      sass:config.scssPath
    .pipe livereload()

gulp.task 'watch', ->
  gulp.watch config.sassPath + '*/**/*.scss', ['compass']

gulp.task 'bower', ->
  bower()
    .pipe gulp.dest config.bowerDir

gulp.task 'default', ['compass', 'watch']

