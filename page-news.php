<?php
/*
Template Name: news
*/
?>

<?php get_header(); ?>

<div class="container" style="margin-top: 10px;">
    <div class="row">
        <h1 style="margin: 0;overflow-x: hidden">
            <img src="<?php echo get_image_path('title_news.jpg', true); ?>" alt="お知らせ"></h1>
        <div class="col-sm-12" style="background: #ebe1da;padding: 30px;">
            <?php
            $paged = get_query_var('paged');
            $args = array(
                'posts_per_page' => 5,
                'category_name' => 'news',
                'orderby' => 'date',
                'order' => 'DESC',
                'paged' => $paged,
                'post_type' => 'post',
                'post_status' => 'publish',
                'suppress_filters' => 0
            );
            $the_query = new WP_Query($args);
            ?>

            <?php if ($the_query->have_posts()): ?>
                <?php while($the_query->have_posts()) : $the_query->the_post(); ?>
                    <article>
                        <div class="row" style="padding: 30px 0;">
                            <div class="col-sm-3 col-md-2 col-md-offset-1">
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    if (has_post_thumbnail()) :
                                        the_post_thumbnail(
                                            'thumbnail',
                                            array('alt' => get_the_title()));
                                    else :
                                        echo "<img class='img-responsive margin-center' src='" . get_image_path("logo.png") . "''>";
                                    endif;
                                    ?>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-9 col-md-8" id="post-<?php the_ID(); ?>"
                                 style="margin-top: 10px;">
                                <div
                                    class="text-center visible-xs-block visible-sm-inline-block visible-md-inline-block visible-lg-inline-block">
                                    <time class="h4"
                                          style="display: inline-block; margin: 0 20px;">
                                        <?php
                                        if (is_ja()) :
                                            the_date("Y年n月j日");
                                        else :
                                            the_date("F jS , Y");
                                        endif;
                                        ?>
                                    </time>
                                </div>
                                <div
                                    class="text-center visible-xs-block visible-sm-inline-block visible-md-inline-block visible-lg-inline-block">
                                    <h2><a style="display: inline-block; margin: 0 20px;"
                                           href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
                                </div>
                                <div style="margin: 30px 0;"><?php the_excerpt(); ?></div>
                                <?php wp_social_bookmarking_light_output_e(null, get_permalink(), the_title("", "", false)); ?>
                            </div>
                        </div>
                        <div class="col-md-10 col-md-offset-1" style="border-bottom: 1px dashed;"></div>
                    </article>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
            <p><?php if (function_exists('pagination')) pagination($the_query->max_num_pages); ?></p>
        </div>
    </div>
</div>


<?php get_sidebar(); ?>

<?php get_footer(); ?>
