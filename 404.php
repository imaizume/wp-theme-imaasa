<!-- 404.php -->
<?php get_header(); ?>
<div class="container" style="margin-top: 10px;">
<div class="row text-center">
  <div class="col-sm-12" style="background: #ebe1da;padding: 30px;">
    <h1>お探しのページは見つかりませんでした</h1>
    <p>URLが間違っているか、ページが移動・削除された可能性があります。</p>
    <p>お手数ですが、URLを再度ご確認いただくかトップページからやり直して下さい。</p>
    <p><a href="<?php echo home_url(); ?>">トップページへ戻る</a></p> 
  </div>
</div>
</div>
<?php get_footer(); ?>
