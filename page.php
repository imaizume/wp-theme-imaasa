<?php get_header(); ?>

<div class="container" style="margin-top: 10px;">

    <?php if (is_front_page()): ?>

        <?php get_template_part('template/content', 'home'); ?>

    <?php else: ?>

        <div class="row">
            <?php if (have_posts()) : while (have_posts()) :
            the_post();
            $slug = get_post()->post_name; ?>

            <h1 style="margin: 0;overflow-x: hidden;"><img
                    src="<?php echo get_image_path('title_' . (($id = get_post()->post_parent) ? get_post($id)->post_name . "_" : "") . $slug . '.jpg', true); ?>"
                    alt="<?php get_the_title(); ?>"></h1>
            <div class="col-sm-12" style="background: #ebe1da;padding: 30px;">
                <?php the_content(__('続きを読む')); ?>
                <?php endwhile; ?>
                <?php else: ?>
                    <div class="navigation">
                        <?php next_posts_link(trim('&laquo; 前へ', 'default')); ?>
                        <?php previous_posts_link(trim('次へ &raquo;', 'default')); ?>
                    </div>
                <?php endif; ?>
                <?php
                if (!is_page(array('confirm', 'complete', 'error'))) :
                    wp_social_bookmarking_light_output_e(null, get_permalink(), the_title("", "", false));
                endif;
                ?>
            </div>
        </div>

    <?php endif; ?>

</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
