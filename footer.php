<footer class="noprint">
    <div class="container">
        <?php if (is_page('confirm')) : ?>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="<?php echo home_url('/'); ?>"
                       style="display: inline-block;vertical-align: middle;float: none;border-radius: 50%;">
                        <?php echo_responsive_image_tag('logo.png', 'すき焼今朝ロゴ'); ?>
                    </a>
                    <a href="<?php echo home_url('/'); ?>"
                       style="display: inline-block;vertical-align: middle; float: none;">
                        <?php echo_responsive_image_tag('logo-type.png', '今朝'); ?>
                    </a>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="back-to-top text-right">
                        <a href="#"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                            <?php echo is_ja() ? 'トップへ戻る' : 'Top of Page' ; ?></a>
                    </div>
                </div>
            </div>

			<div class="row">
                <div class="col-xs-12 visible-xs text-center">
					<a href="https://chienotomoshibi.jp/intro/imaasa/" style="display: inline-block;margin-top: 10px;" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/chienotomoshibi_small.jpg"
							 alt="智慧の燈火 明治から伝わる今朝の味わい" class='img-responsive'>
					</a>
				</div>
                <div class="col-sm-6 visible-sm text-center">
					<div class="fb-page" style="height: 360px;"
						 data-href="https://www.facebook.com/%E3%81%99%E3%81%8D%E7%84%BC-%E4%BB%8A%E6%9C%9D-214760231889043/"
						 data-tabs="timeline" data-width="350" data-height="360" data-small-header="true"
						 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
					<a href="https://chienotomoshibi.jp/intro/imaasa/" style="display: inline-block;margin-top: 10px;" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/chienotomoshibi_small.jpg"
							 alt="智慧の燈火 明治から伝わる今朝の味わい" class='img-responsive'>
					</a>
				</div>
                <div class="col-sm-8 visible-md visible-lg text-center">
					<div class="fb-page" style="height: 360px;"
						 data-href="https://www.facebook.com/%E3%81%99%E3%81%8D%E7%84%BC-%E4%BB%8A%E6%9C%9D-214760231889043/"
						 data-tabs="timeline" data-width="480" data-height="360" data-small-header="true"
						 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
					<a href="https://chienotomoshibi.jp/intro/imaasa/" style="display: inline-block;margin-top: 10px;" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/chienotomoshibi_small.jpg"
							 alt="智慧の燈火 明治から伝わる今朝の味わい" class='img-responsive'>
					</a>
				</div>

                <div class="col-xs-12 col-sm-6 col-md-4" style="margin-top: 30px;">
                    <div class="text-center" style="margin-bottom: 10px;">

                        <a href="https://resources.imaasa.com/wordpress/wp-content/uploads/2016/03/26193602/leaflet.pdf"
                           style="display: inline-block;">
                            <img src="<?php echo get_image_path("pdf.png"); ?>" alt="パンフレット" class="media-object"
                                 style="max-width: 60px;">
                        </a>
                        <p>
                            <?php if (is_ja()) : ?>
                                <a href="https://resources.imaasa.com/wordpress/wp-content/uploads/2016/03/26193602/leaflet.pdf"
                                   target="_blank" style="display: inline-block;font-weight: bold;">こちら</a>から電子版パンフレットがご覧いただけます
                            <?php else : ?>
                                Get our pdf leaflet <a
                                    href="https://resources.imaasa.com/wordpress/wp-content/uploads/2016/03/26193602/leaflet.pdf"
                                    target="_blank" style="display: inline-block;font-weight: bold;">HERE</a>
                            <?php endif; ?>
                        </p>
                    </div>
                    <div class="text-center" style="margin-bottom: 10px;">
                        <a href="<?php echo home_url("access"); ?>"
                           style="display: inline-block;">
                            <img src="<?php echo get_image_path("coupon-banner.jpg", true); ?>" alt="今朝クーポンプレゼント"
                                 class="img-responsive">
                        </a>
                    </div>
                    <?php if (is_ja()) : ?>
                        <div class="text-center" style="margin-bottom: 10px;">
                            <div id="TA_cdsratingsonlynarrow616" class="TA_cdsratingsonlynarrow"
                                 style="display: inline-block;">
                                <ul id="dOfGawCV" class="TA_links Dy5d0P">
                                    <li id="yHQJtw" class="9nfjn7sH">
                                        <a target="_blank" href="https://www.tripadvisor.jp/"><img
                                                src="https://www.tripadvisor.jp/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png"
                                                alt="TripAdvisor"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="text-center" style="margin-bottom: 10px;">
                            <div id="TA_cdsratingsonlynarrow578" class="TA_cdsratingsonlynarrow"
                                 style="display: inline-block;">
                                <ul id="gH6HuJbfRB" class="TA_links 2hHjutZbgu">
                                    <li id="4vHMoSae6w" class="3ZJJlo5">
                                        <a target="_blank" href="https://www.tripadvisor.com/"><img
                                                src="https://www.tripadvisor.com/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png"
                                                alt="TripAdvisor"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
					<?php endif; ?>

<!--Social Buttons-->
					<?php
						$title = "";
						$title = is_ja() ? 'すき焼今朝' : 'Sukiyaki Restaurant Ima-Asa' ;
						if (!is_front_page()) :
							$title = wp_title('') . ' | ' . $title;
						endif;
						$title_encode = urlencode($title); // 記事タイトルエンコード
						$canonical_url_encode = urlencode(get_permalink()); // 記事URLエンコード
					?>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '263886087155232',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.1'
    });

    FB.AppEvents.logPageView();

  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
					<div class="share-buttons">
						<div class="share-button fb-like">
							<a class="fb-like" onclick="FB.ui({ method: 'share', display: 'popup', href: '<?php echo get_permalink(); ?>', }, function(response){});">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
						</div>
						<div class="share-button tweet">
							<a class="tweet" href="//twitter.com/intent/tweet?url=<?php echo $canonical_url_encode;?>&text=<?php echo $title_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</div>
						<div class="share-button hatenabookmark">
							<a class="hatenabookmark" href="//b.hatena.ne.jp/add?mode=confirm&url=<?php echo $canonical_url_encode;?>&title=<?php echo $title_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=510');return false;">
							<span class="hatenabookmark-squared-ffffff"></span>
							</a>
						</div>
					</div>

                    <div class="text-center" style="margin: 30px auto;">
                        <p><a href="//feeds.feedburner.com/imaasa/feed" rel="alternate" type="application/rss+xml"><img src="//feedburner.google.com/fb/images/pub/feed-icon32x32.png" alt="" style="vertical-align:middle;border:0"/></a>&nbsp;<a href="https://feeds.feedburner.com/imaasa/feed" rel="alternate" type="application/rss+xml">
                    <?php if (is_ja()) : ?>
RSS登録はこちら
                    <?php else : ?>
Subscribe RSS
                    <?php endif; ?>
</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4" style="padding: 30px;text-align: center;">
                    <a href="<?php echo home_url('/'); ?>"
                       style="display: inline-block;vertical-align: middle;float: none;border-radius: 50%;">
                        <?php echo_responsive_image_tag('logo.png', 'すき焼今朝ロゴ'); ?>
                    </a>
                    <a href="<?php echo home_url('/'); ?>"
                       style="display: inline-block;vertical-align: middle; float: none;">
                        <?php echo_responsive_image_tag('logo-type.png', '今朝'); ?>
                    </a>
                </div>
                <div class="col-sm-12 col-md-8" style="padding: 30px;text-align: center;line-height: 2em;">
                    <ul class="row list-inline">
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('about'); ?>"><?php echo is_ja() ? '今朝について' : 'About Imaasa'; ?></a>
                        </li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('menu/dinner'); ?>"><?php echo is_ja() ? 'お品書き' : 'Menu'; ?></a>
                        </li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('reservation'); ?>"><?php echo is_ja() ? 'ご予約' : 'Reservation'; ?></a>
                        </li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('news'); ?>"><?php echo is_ja() ? 'お知らせ' : 'News'; ?></a>
                        </li>
                    </ul>
                    <ul class="row list-inline">
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('access'); ?>"><?php echo is_ja() ? '店舗案内' : 'Access'; ?></a></li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('recruit'); ?>"><?php echo is_ja() ? 'スタッフ募集' : 'Recruit'; ?></a></li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a
                                href="<?php echo home_url('contact'); ?>"><?php echo is_ja() ? 'お問い合わせ' : 'Contact'; ?></a></li>
                        <li class="col-xs-12 col-sm-6 col-md-3"><a href="<?php echo home_url('/'); ?>"><?php echo is_ja() ? 'トップページ' : 'Home'; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <small class="copyright" style="display: block;text-align: center;">
                        COPYRIGHTS &copy; 2014 IMAASA ALL RIGHTS RESERVED.
                    </small>
                </div>
            </div>
        <?php endif; ?>
    </div>
</footer>

<script src="//code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="<?php echo get_script_path('smoothscroll.js'); ?>"></script>
<script src="//itra.jp/jquery_socialbutton_plugin/js/jquery.socialbutton-1.9.1.min.js"></script>

<?php if (is_ja()) : ?>
    <script
        src="https://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=616&amp;locationId=1678381&amp;lang=ja&amp;border=true&amp;shadow=false&amp;backgroundColor=white&amp;display_version=2"></script>
<?php else : ?>
    <script
        src="https://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=578&amp;locationId=1678381&amp;lang=en_US&amp;border=true&amp;shadow=false&amp;backgroundColor=white&amp;display_version=2"></script>
<?php endif; ?>
<?php if (is_page('reservation') || (is_page('error') && get_post($post->post_parent)->post_name == 'reservation')) : ?>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <?php if (is_ja()) : ?>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
    <?php endif; ?>
    <script src="<?php echo get_script_path('datepicker.js'); ?>"></script>
<?php endif; ?>

<?php if (is_page(array('reservation', 'contact', 'error'))) : ?>
    <script src="<?php echo get_script_path('jquery.autoKana.js', 'autokana'); ?>"></script>
    <script src="<?php echo get_script_path('autokana.js'); ?>"></script>
<?php endif; ?>

<script src="<?php echo get_script_path('bootstrap.min.js', 'bootstrap/js'); ?>"></script>
<div id="fb-root"></div>
<?php if (is_ja()) : ?>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5&appId=388303844695942";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php else : ?>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=388303844695942";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
